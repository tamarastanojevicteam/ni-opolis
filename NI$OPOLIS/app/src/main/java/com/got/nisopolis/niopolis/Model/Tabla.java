package com.got.nisopolis.niopolis.Model;

import java.util.List;

/**
 * Created by TAMARA on 16.04.2017..
 */

public class Tabla {

    private Polje polja[];
    private long novcanik;
    private Lista_Kartica nekretnine;
    private Lista_Sansi sanse;
    private Lista_Iznenadjenja iznenadjenja;

    public Tabla()
    {
        polja=new Polje[40];
        nekretnine=Lista_Kartica.getInstance();
        sanse=Lista_Sansi.getInstance();
        iznenadjenja=Lista_Iznenadjenja.getInstance();
        InicijalizujPolja();
        novcanik=0;
    }

    public Polje[] getPolja() {
        return polja;
    }

    public long getNovcanik() {
        return novcanik;
    }

    public void setNovcanik(long novcanik) {
        this.novcanik = novcanik;
    }

    public Lista_Kartica getNekretnine() {
        return nekretnine;
    }

    public Lista_Sansi getSanse() {
        return sanse;
    }

    public Lista_Iznenadjenja getIznenadjenja() {
        return iznenadjenja;
    }




    void InicijalizujPolja()
    {
        polja[0]=new Start();
        polja[1]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(0));
        polja[2]=sanse.getInstance();
        polja[3]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(1));
        polja[4]=new PorezNaPlatu();
        polja[5]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(2));
        polja[6]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(3));
        polja[7]=iznenadjenja.getInstance();
        polja[8]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(4));
        polja[9]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(5));
        polja[10]=new Prolaz();

        polja[11]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(6));
        polja[12]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(7));
        polja[13]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(8));
        polja[14]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(9));
        polja[15]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(10));
        polja[16]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(11));
        polja[17]=sanse.getInstance();
        polja[18]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(12));
        polja[19]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(13));

        polja[20]=new Parking();
        polja[21]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(14));
        polja[22]=iznenadjenja.getInstance();
        polja[23]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(15));
        polja[24]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(16));
        polja[25]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(17));
        polja[26]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(18));
        polja[27]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(19));
        polja[28]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(20));
        polja[29]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(21));
        polja[30]=new Zatvor();

        polja[31]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(22));
        polja[32]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(23));
        polja[33]=sanse.getInstance();
        polja[34]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(24));
        polja[35]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(25));
        polja[36]=iznenadjenja.getInstance();
        polja[37]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(26));
        polja[38]=new PorezNaLuksuz();
        polja[39]=new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(27));
    }

    public void OslobodiPolja(Player p)
    {
        for(int i=0; i<40;i++)
        {
            Polje polje = polja[i];
            if(polje instanceof ObicnoPolje)
            {
                ObicnoPolje op = (ObicnoPolje) polja[i];
                if(!op.free)
                {
                    if(p.getIP().equals(op.player.getIP()))
                    {
                        op.free=true;
                        op.player=null;
                    }
                }
            }
        }

    }


    public String Odigraj(Player p)
    {
        String odgovor="";
        Polje st;
        if(p.proveraHipo()){
            ispao(p);
        }

        st=polja[p.getPozicija()];
        if(st instanceof ObicnoPolje)
        {


            ObicnoPolje op=(ObicnoPolje) st;
            if(op.IsFree()) {
                if (op.kartica instanceof Nekretnina) {
                    Nekretnina nk = (Nekretnina) op.kartica;
                    int poz = nekretnine.getInstance().lista_kartica.indexOf(nk);
                    odgovor = "nekretnina " + poz;
                } else {
                    JavnaPreduzeca jp = (JavnaPreduzeca) op.kartica;
                    int poz = nekretnine.getInstance().lista_kartica.indexOf(jp);
                    odgovor = "javno_preduzece " + poz;
                }
            }
            else
            {
                if(op.player.getIP().equals(p.getIP()))
                {
                    odgovor="zauzeto ja";
                }
                else
                {
                    long cena;
                    if (op.kartica instanceof Nekretnina) {
                        Nekretnina nk = (Nekretnina) op.kartica;
                        op.player.setNovac(nk.getCena());
                        p.setNovac(-nk.getCena());
                        cena=nk.getCena();
                    } else {
                        JavnaPreduzeca jp = (JavnaPreduzeca) op.kartica;
                        op.player.setNovac(jp.getCena());
                        p.setNovac(-jp.getCena());
                        cena=jp.getCena();
                    }

                    odgovor="zauzeto "+cena;
                }

            }
        }
        else if(st instanceof Lista_Iznenadjenja)
        {
            Iznenadjenje iz=iznenadjenja.getInstance().Izvuci_Iznenadjenje();
            int x=iznenadjenja.getInstance().spil_iznenadjenja.indexOf(iz);
            p.izvuciIznenadjenje(x);
            odgovor="iznenadjenje "+x;
        }
        else if(st instanceof Lista_Sansi)
        {
            Sansa san=sanse.getInstance().Izvuci_Sansu();
            int x=sanse.getInstance().spil_sansi.indexOf(san);
            p.izvuciSansu(x);
            odgovor="sansa "+x;
        }
        else if(st instanceof Start)
        {
            Start str=(Start) st;
            odgovor="start "+str.getCena();
        }
        else if(st instanceof Prolaz)
        {
            odgovor="prolaz";
        }
        else if(st instanceof Zatvor)
        {
            p.zatvor=true;
            p.brKrugZatvor=3;
            p.setPozicijaZatvor();
            odgovor="zatvor";
        }
        else if(st instanceof Parking)
        {
            p.novac+=novcanik;
            odgovor="parking "+novcanik;
            novcanik=0;
        }
        else if(st instanceof PorezNaLuksuz)
        {
            p.novac-=20000;
            setNovcanik(novcanik+20000);
            odgovor="porez "+20000;
        }
        else if(st instanceof PorezNaPlatu)
        {
            p.novac-=20000;
            setNovcanik(novcanik+20000);
            odgovor="porez "+20000;
        }

        return odgovor;
    }

    public String kupi(Player p, int poz) {
        String odgovor="";
        ObicnoPolje op = new ObicnoPolje(nekretnine.getInstance().lista_kartica.get(poz));
        Kartica k1=op.getKartica();
        for (int i = 0; i < 40; i++) {
            if (polja[i] instanceof ObicnoPolje) {
                ObicnoPolje oo = (ObicnoPolje) polja[i];
                Kartica k2=oo.getKartica();
                if(k2 instanceof Nekretnina && k1 instanceof Nekretnina)
                {
                    Nekretnina n1=(Nekretnina)k1;
                    Nekretnina n2=(Nekretnina)k2;

                    if(n1.getNaziv().equals(n2.getNaziv()))
                    {
                        oo.free = false;
                        oo.player = p;
                        odgovor = "kupljeno " + poz;
                        return odgovor;
                    }
                }
                else if(k2 instanceof JavnaPreduzeca && k1 instanceof JavnaPreduzeca)
                {
                    JavnaPreduzeca j1=(JavnaPreduzeca) k1;
                    JavnaPreduzeca j2=(JavnaPreduzeca)k2;

                    if(j1.getNaziv().equals(j2.getNaziv()))
                    {
                        oo.free = false;
                        oo.player = p;
                        odgovor = "kupljeno " + poz;
                        return odgovor;
                    }

                }
            }
        }

        return odgovor;

    }


    public String prodajKarticu(int poz){
        String odgovor="";
        Kartica kart=nekretnine.getKartica(poz);
        ObicnoPolje zaBrisanje=new ObicnoPolje(kart);
        Kartica k1=zaBrisanje.kartica;

        for (int i = 0; i < 40; i++) {
            if (polja[i] instanceof ObicnoPolje) {
                ObicnoPolje oo = (ObicnoPolje) polja[i];
                Kartica k2 = oo.kartica;
                if (k2 instanceof Nekretnina && k1 instanceof Nekretnina) {
                    Nekretnina n1 = (Nekretnina) k1;
                    Nekretnina n2 = (Nekretnina) k2;

                    if (n1.getNaziv().equals(n2.getNaziv())) {
                        oo.free = true;
                        oo.player = null;
                        odgovor = "prodato "+poz;
                        return odgovor;
                    }
                } else if (k2 instanceof JavnaPreduzeca && k1 instanceof JavnaPreduzeca) {
                    JavnaPreduzeca j1 = (JavnaPreduzeca) k1;
                    JavnaPreduzeca j2 = (JavnaPreduzeca) k2;

                    if (j1.getNaziv().equals(j2.getNaziv())) {
                        oo.free = true;
                        oo.player = null;
                        odgovor = "prodato " + poz;
                        return odgovor;
                    }

                }
            }
        }
        return odgovor;
    }


    public String prodaj(int poz, String sta) {
        String odgovor = "";
        Kartica kart = nekretnine.getKartica(poz);
        ObicnoPolje op = new ObicnoPolje(kart);
        Kartica k1 = op.kartica;
        Nekretnina n1 = (Nekretnina) k1;

        for (int i = 0; i < 40; i++) {
            if (polja[i] instanceof ObicnoPolje) {
                ObicnoPolje oo = (ObicnoPolje) polja[i];
                Kartica k2 = oo.kartica;
                if (k2 instanceof Nekretnina) {
                    Nekretnina n2 = (Nekretnina) k2;
                    if (sta.equals("kuca")) {
                        n2.broj_kuca--;
                        odgovor = "prodato kuca " + poz;
                        return odgovor;
                    } else if (sta.equals("hotel"))
                        n2.daLiImaHotel = false;
                    odgovor = "prodato hotel " + poz;
                    return odgovor;
                }
            }
        }
        return odgovor;
    }


    public String hipoteka(Player p, int poz){
        String odgovor="hipoteka ok "+poz;

        p.postaviHipo(poz);

        return odgovor;
    }

    public void ispao(Player p){

        List<Kartica> lk=p.getMojekartice();
        for(int i=0; i<40; i++){
            if (polja[i] instanceof ObicnoPolje) {
                ObicnoPolje oo = (ObicnoPolje) polja[i];
                Kartica k2=oo.getKartica();
                for (int j = 0; j < lk.size(); j++) {
                    Kartica k1=lk.get(j);
                    if(k2 instanceof Nekretnina && k1 instanceof Nekretnina)
                    {
                        if(k2.equals(k1))
                        {
                            oo.free=true;
                            oo.player=null;

                        }
                    }
                    else if(k2 instanceof JavnaPreduzeca && k1 instanceof JavnaPreduzeca) {
                        if(k2.equals(k1)){
                            oo.free=true;
                            oo.player=null;
                        }
                    }
                }
            }
        }
    }

}
