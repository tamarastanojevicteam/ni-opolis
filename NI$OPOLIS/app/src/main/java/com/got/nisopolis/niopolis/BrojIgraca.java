package com.got.nisopolis.niopolis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

public class BrojIgraca extends AppCompatActivity {

    Spinner sp;
    Button b;

    void next()
    {
        sp=(Spinner) findViewById(R.id.spinner);

        b=(Button) findViewById(R.id.btnSpinDalje);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a=sp.getSelectedItem().toString();
                Intent i = new Intent(BrojIgraca.this, Prava_Tabla.class);
                i.putExtra("broj",a);
                startActivity(i);
            }
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broj_igraca);

        next();
    }
}
