package com.got.nisopolis.niopolis.Model;

/**
 * Created by ProBook on 4/16/2017.
 */

public class Iznenadjenje extends Kartica {

    protected String tip;
    protected String textIznenadjenja;
    protected boolean izvuceno;

    public String getTextIznenadjenja()
    {
        return textIznenadjenja;
    }

    public void setTextIznenadjenja(String x)
    {
        this.textIznenadjenja=x;
    }

}
